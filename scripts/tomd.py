#!/usr/bin/python3

import sys
import os
import json
import collections

#
# Note we use oyaml which is a drop-in replacement for PyYAML which preserves
# dict ordering. And you have to install PyYAML first, then have a try, e.g.
# ```bash
#    $ git clone https://github.com/wimglenn/oyaml.git /tmp/oyaml
#    $ export PYTHONPATH=/tmp/oyaml:$PYTHONPATH
# ```
#
try:
    import oyaml as yaml
except ModuleNotFoundError:
    import yaml


def txt2yaml(txt, indent=2):
    # XXX: yaml support to load both JSON and YAML
    obj = yaml.safe_load(txt)
    out = yaml.dump(obj, default_flow_style=False, indent=indent)
    return out.rstrip('\n')


def obj2json(obj, indent=2):
    out = json.dumps(obj, indent=indent)
    return out


def obj2yaml(obj, indent=2):
    # XXX: yaml support to load both JSON and YAML
    out = yaml.dump(obj, default_flow_style=False, indent=indent)
    return out.rstrip('\n')


def kv_safe_get(udict, key):
    if udict is None:
        return None

    if key not in udict:
        return None
    val = udict[key]
    if val is None:  # XXX: in case val is set as 'null'
        return None
    if not val:      # XXX: in case val is an empty string
        return None
    return val


def kv_safe_get_rcrs(udict, key_str):
    l_key = key_str.split('.')
    for key in l_key[:-1]:
        if udict is None:
            return None
        if key not in udict:
            return None
        udict = udict[key]
    return kv_safe_get(udict, l_key[-1])


def load_obj_byyamlfile(yaml_file):
    txt = None
    with open(yaml_file, 'r') as file_handle:
        txt = ''.join(file_handle.readlines())
    obj = yaml.safe_load(txt)
    return obj


def get_keywords_md(keywords):
    l_out = []
    for keyword in keywords:
        l_out.append("`%s`" % keyword)
    return '; '.join(l_out)


def dump(films):
    for film in films:
        film_url = film['url']
        film_iid = film['iid']
        film_udate = film['udate']
        film_subject = film['subject']
        film_keywords = film['keywords']
        film_desc = film['desc']
        file_comments = kv_safe_get(film, 'comments')
        print("# %s - [%s](%s)" % (film_iid, film_subject, film_url))
        print("* **关键字  :** %s" % get_keywords_md(film_keywords))
        print("* **更新时间:** %s" % film_udate)
        print("* **视频简介:** %s" % film_desc)
        if file_comments is not None:
            print("* **关联信息:** %s" % file_comments)
        print('---')
        print()


def main(argc, argv):
    if argc < 2:
        print("Usage: %s <yaml file> ..." % argv[0], file=sys.stderr)
        return 1

    yaml_files = argv[1:]

    for yaml_file in yaml_files:
        obj = load_obj_byyamlfile(yaml_file)
        dump(obj)

    return 0


if __name__ == '__main__':
    sys.exit(main(len(sys.argv), sys.argv))
